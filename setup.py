from setuptools import setup

import os

if os.path.exists("README.txt"):
    readme = open("README.txt")
else:
    print "Warning: using markdown README"
    readme = open("README.md")

setup(
    name="django-exportable",
    version="0.2",
    description="Django export data",
    long_description=readme.read(),
    author="Luis Velez",
    author_email="lvelezsantos@gmail.com",
    license="BSD",
    url="https://bitbucket.org/lvelezsantos/django-exportable/",
    install_requires=(
        "Django >= 1.6",
    ),
    packages=['django_exportable'],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
)
