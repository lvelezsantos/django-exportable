Django Exportable
==================

Django Exportable.

Features
--------

 - Export models data.
    - CSV


How to Use
----------

 1. Get and install the package:

        pip install django-exportable

    or

        git clone git@bitbucket.org:lvelezsantos/django-exportable.git django-exportable

        cd django-exportable

        python setup.py install



License
-------

Licence: BSD. See included `LICENSE` file.

Note that this license applies to this repository only.