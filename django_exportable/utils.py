# -*- encoding: utf-8 -*-
from _csv import register_dialect
from collections import OrderedDict

from datetime import date
from django.utils.translation import ugettext as _
import codecs
import csv
import cStringIO


class DialectCsv(csv.excel):
    delimiter = ';'
register_dialect('dialect_csv', DialectCsv)


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=DialectCsv, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        row_to_write = []

        for s in row:
            row_to_write.append(s.encode("utf-8"))

        self.writer.writerow(row_to_write)
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


class ExportDataBase():

    def __init__(self, model, queryset, fields_to_export=None, fields_to_exclude=None, replace_data=None, *args, **kwargs):
        """

        :param model:   Model to export
        :param queryset:    Queryset to export
        :param fields_to_export:  tuplw fields to export (('field', 'Label'), ('field', 'Label'))
        :param fields_to_exclude:   list ['field']
        :param args:
        :param kwargs:
        :return:
        """
        self.model = model
        self.queryset = queryset
        self.fields_to_export = fields_to_export

        self.replace_data = replace_data.copy() if replace_data is not None else None

        if fields_to_export is not None:
            self.fields_to_export = OrderedDict(fields_to_export)

        if fields_to_exclude is not None:
            if not isinstance(fields_to_exclude, list):
                raise Exception('fields_to_exclude not list')

        self.fields_to_exclude = fields_to_exclude

    def get_fields_to_exclude(self):

        return self.fields_to_exclude if self.fields_to_exclude is not None else []

    def get_model_fields(self):

        fields = [(field.name, field.verbose_name) for field in self.model._meta.fields]

        fields = OrderedDict(fields)

        return fields

    def get_fields(self):
        """
        Search models fields from Model
        :param model:
        :return [{name_field: verbose_name_field}]:
        """

        if self.fields_to_export is None:
            # for field in self.model._meta.fields:
            #     if isinstance(field, models.ForeignKey):
            #         print field.rel.to._meta.fields
            # TODO: get foreign keys fields
            fields = self.get_model_fields()

        else:
            fields = self.fields_to_export.copy()

        self.exclude_fields(fields)

        if not 'id' in fields:

            old_fields = fields.copy()

            fields = OrderedDict({'id': 'ID'})

            fields.update(old_fields)


        return fields

    def exclude_fields(self, fields):
        for key in self.get_fields_to_exclude():
            if key in fields:
                del fields[key]

    def get_data_to_export(self, queryset=None):

        if queryset is None:
            queryset = self.queryset

        result = queryset.values_list(*self.get_fields().keys())
        result = list(result)
        return result

    def export_file(self, fileobj):
        raise NotImplementedError()

    def change_data(self, data):
        result_data = []
        i = 0
        for s in data:

            if isinstance(s, (int, float)):
                s = u'{0}'.format(s)

            if isinstance(s, date):
                try:
                    s = s.strftime('%d/%m/%Y')
                except ValueError:
                    s = ''

            if isinstance(s, bool):
                if s:
                    s = _('Yes')

                else:
                    s = _('No')

            if s is None:
                s = u''

            if self.replace_data is not None:
                if i in self.replace_data:

                    rep_string = u'{0}'.format(s)
                    try:
                        rep_int = int(s)

                    except ValueError:
                        rep_int = ''

                    try:
                        s = self.replace_data[i][rep_int]

                    except KeyError:
                        try:
                            s = self.replace_data[i][rep_string]
                        except KeyError:
                            s = ''

            result_data.append(s)
            i += 1

        return result_data

    def replace_index_in_data(self):
        if self.replace_data is not None:
            i = 0
            new_replace_data = {}
            for field in self.get_fields().keys():
                if field in self.replace_data:
                    # print field
                    # print self.replace_data[field]
                    new_replace_data[i] = self.replace_data[field]

                i += 1

            self.replace_data = new_replace_data

    def update_fields_to_export(self, update_fields):
        """ Delete fields wit not in update_fields. Execute before replace_index_in_data function.
        :param update_fields:
        :return:
        """
        if isinstance(update_fields, (list, tuple)):
            fields_to_export = self.get_fields()

            for field in fields_to_export.keys():

                if not field in update_fields:
                    del fields_to_export[field]

            self.fields_to_export = fields_to_export

class ExportDataCSV(ExportDataBase):

    def export_file(self, fileobj):

        writer = UnicodeWriter(fileobj)

        self.replace_index_in_data()

        writer.writerow(self.get_fields().values())

        for row_data in self.get_data_to_export():
            row_data = self.change_data(row_data)
            # print row_data
            writer.writerow(row_data)

        return fileobj

