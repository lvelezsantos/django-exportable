from django.utils.translation import ugettext as _
from django import forms


class SelectFieldsForm(forms.Form):

    fields_to_export = forms.MultipleChoiceField(
        label=_('Select Fields to export'),
        widget=forms.CheckboxSelectMultiple()
    )

    def __init__(self, choices=(), *args, **kwargs):

        super(SelectFieldsForm, self).__init__(*args, **kwargs)

        self.fields['fields_to_export'].choices = choices