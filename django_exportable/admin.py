from django.contrib.admin import helpers
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from django.utils.encoding import force_unicode
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin, messages

from django.db import models
from django_exportable.forms import SelectFieldsForm
from django_exportable.utils import ExportDataBase, ExportDataCSV


class ExportableAdmin(admin.ModelAdmin):

    fields_to_export = None
    fields_to_exclude = None
    defaul_filename_to_export = 'export_data'
    replace_date = None
    export_template = None

    def get_replace_data(self, request):
        return self.replace_date

    def get_actions(self, request):
        """
        Add export action to list admin actions
        """
        actions = super(ExportableAdmin, self).get_actions(request)

        actions.update(
            {
                'export': self.get_action('export'),
                'export_without_preview': self.get_action('export_without_preview'),
            }
        )

        return actions

    def get_fields_to_export(self, request):
        """
        Obtain field to export data
        :return:
        """

        return self.fields_to_export

    def get_fields_to_exclude(self, request):
        """
        Obtain exclude field to export data
        :return:
        """

        return self.fields_to_exclude if self.fields_to_exclude is not None else []

    def export_without_preview(self, request, queryset):

        return self.export(request, queryset, with_preview=False)
    export_without_preview.short_description = _("Export selected/es %(verbose_name_plural)s without preview")

    def export(self, request, queryset, with_preview=True):
        """
        Restores an element and all its child elements

        :type request: object
        :param request:
        :param queryset:
        """

        if not request.user.is_staff:
            return PermissionDenied

        count = queryset.count()

        if count == 0:
            messages.error(request, _("No items to export"))
            return None

        opts = self.model._meta
        app_label = opts.app_label

        if request.POST.get('post'):

            filename = self.defaul_filename_to_export

            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="{0}.csv"'.format(filename)

            export = ExportDataCSV(
                model=self.model,
                queryset=queryset,
                fields_to_export=self.get_fields_to_export(request),
                fields_to_exclude=self.get_fields_to_exclude(request),
                replace_data=self.get_replace_data(request)
            )

            form = SelectFieldsForm(choices=export.get_fields().items(), data=request.POST)
            if form.is_valid():
                update_fields = form.cleaned_data.get('fields_to_export')
                export.update_fields_to_export(update_fields)
                print
                # export.replace_data()
                return export.export_file(response)

        else:
            export = ExportDataBase(
                model=self.model,
                queryset=queryset,
                fields_to_export=self.get_fields_to_export(request),
                fields_to_exclude=self.get_fields_to_exclude(request),
                replace_data=self.get_replace_data(request)
            )

            export.replace_index_in_data()

            form = SelectFieldsForm(
                choices=export.get_fields().items(),
                initial={
                    'fields_to_export': export.get_fields().keys()
                }
            )

        if count == 1:
            objects_name = force_unicode(opts.verbose_name)

        else:
            objects_name = force_unicode(opts.verbose_name_plural)

        fields_to_export = export.get_fields()


        title = _("Export selected/es %(name)s") % {"name": objects_name}

        data_to_export = export.get_data_to_export()

        context = {
            'column_titles': fields_to_export.values(),
            'data_to_export': data_to_export,
            "title": title,
            "objects_name": objects_name,
            'queryset': queryset,
            "opts": opts,
            "app_label": app_label,
            'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            'form': form,
            'with_preview': with_preview
        }

        # Display the confirmation page
        return TemplateResponse(request, self.export_template or [
            "admin/%s/%s/export.html" % (app_label, opts.object_name.lower()),
            "admin/%s/export.html" % app_label,
            "admin/export.html"
        ], context,)
    export.short_description = _("Export selected/es %(verbose_name_plural)s")
